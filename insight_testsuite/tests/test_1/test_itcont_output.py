import unittest
import sys
sys.path.insert(0,'../../../src')
import filecmp
import find_political_donors as fpd


class MyTest(unittest.TestCase):
    def test_files(self):
        fpd.output_donor_files(r'input/itcont.txt', r'output/medianvals_by_zip.txt', r'output/medianvals_by_date.txt')
        self.assertEqual(filecmp.cmp(r'output/ground_truth_medianvals_by_zip.txt',
                         r'output/medianvals_by_zip.txt'),True)
        self.assertEqual(filecmp.cmp(r'output/ground_truth_medianvals_by_date.txt',
                                     r'output/medianvals_by_date.txt'), True)

if __name__ == '__main__':
    unittest.main()
