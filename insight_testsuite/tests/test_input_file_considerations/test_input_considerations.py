import unittest
import sys
sys.path.insert(0,'../../../src')
import find_political_donors as fpd
import os
import filecmp


class MyTest(unittest.TestCase):
    def test_OTHER_ID_empty(self):
        '''Test OTHER_ID containing a non-empty value'''
        df = fpd.create_contrib_df(r'input/itcont_first_line.txt')
        self.assertEqual(df.shape[0],0)

    def test_TRANSACTION_DT_invalid(self):
        '''
        Test TRANSACTION_DT being an invalid date: medianvals_by_zip.txt should
        be unchanged but it should be skipped for medianvals_by_date.txt
        '''
        fpd.output_donor_files(r'input/itcont_transaction_dt_invalid.txt',
                               r'output/medianvals_by_zip_dt_invalid.txt',
                               r'output/medianvals_by_date_dt_invalid.txt')
        self.assertEqual(filecmp.cmp(r'output/medianvals_by_zip_dt_invalid.txt',
                                     r'output/ground_truth_medianvals_by_zip.txt'),True)
        self.assertEqual(os.stat(r'output/medianvals_by_date_dt_invalid.txt').st_size,0)
        os.remove(r'output/medianvals_by_zip_dt_invalid.txt')
        os.remove(r'output/medianvals_by_date_dt_invalid.txt')

    def test_zipcode_invalid(self):
        '''
        Test if the zipcode is an invalid date: medianvals_by_date.txt should
        be unchanged but medianvals_by_zip.txt should skip the invalid zipcodes
        '''
        fpd.output_donor_files(r'input/itcont_zip_invalid.txt',
                               r'output/medianvals_by_zip_zip_invalid.txt',
                               r'output/medianvals_by_date_zip_invalid.txt')
        self.assertEqual(filecmp.cmp(r'output/medianvals_by_date_zip_invalid.txt',
                                     r'output/ground_truth_medianvals_by_date.txt'),True)
        self.assertEqual(os.stat(r'output/medianvals_by_zip_zip_invalid.txt').st_size,0)
        os.remove(r'output/medianvals_by_zip_zip_invalid.txt')
        os.remove(r'output/medianvals_by_date_zip_invalid.txt')

if __name__ == '__main__':
    unittest.main()




