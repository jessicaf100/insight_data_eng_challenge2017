#! /usr/bin/env python

import numpy as np
import pandas as pd
from datetime import datetime
import sys

def create_contrib_df(itcont_filepath):
    '''
    This function creates a dataframe of the relevant donor data from existing information
    :param itcont_filepath: File path to the text file
    :return: contrib_df: Dataframe containing the relevant donor data
    '''
    itcont_contents = open(itcont_filepath, 'r')
    contrib_df = pd.DataFrame(columns=['CMTE_ID',
                                       'ZIP_CODE',
                                       'TRANSACTION_DT',
                                       'TRANSACTION_AMT',
                                       'OTHER_ID',
                                       'TRANSACTION_DATETIME'])

    while True:
        line = itcont_contents.readline()
        fields = line.split('|')
        if len(fields) == 21:
            try:
                transaction_amt_float = float(fields[14])
            except:
                transaction_amt_float = None
            if (len(fields[0]) > 0) and (transaction_amt_float is not None):
                # zip code, make sure that it is a sequence of numbers
                if len(fields[10]) >= 5:
                    try:
                        zip_int = int(fields[10][:5])
                        zip_val = fields[10][:5]
                    except:
                        zip_val = None
                else:
                    zip_val = None

                # make sure the transaction date is a valid datetime
                if len(fields[13]) == 8:
                    try:
                        transaction_datetime = datetime(year=int(fields[13][4:8]),
                                                        month=int(fields[13][0:2]),
                                                        day=int(fields[13][2:4]))
                        transaction_date_int = fields[13]
                    except:
                        transaction_datetime = None
                        transaction_date_int = None
                else:
                    transaction_datetime = None
                    transaction_date_int = None

                contrib_df = pd.concat((contrib_df,
                                        pd.DataFrame([[fields[0],
                                                       zip_val,
                                                       transaction_date_int,
                                                       transaction_amt_float,
                                                       fields[15],
                                                       transaction_datetime]],
                                                     columns=contrib_df.columns)))
        if not line: break
    itcont_contents.close()
    contrib_df = contrib_df[contrib_df['OTHER_ID'].str.len() == 0]
    return contrib_df


def create_by_zip_df(contrib_df):
    '''
    This function creates a dataframe containing a running count of the medians,
    numbers of contributors, and total contribution amount grouped by zip code.
    :param contrib_df: Dataframe containing the relevant donor data
    :return: median_by_zip: Dataframe containing the running counts and totals by zip code.
    '''
    by_zip_subset = contrib_df[contrib_df['ZIP_CODE'].str.len() == 5].copy()
    rolling_median = []
    rolling_sum = []
    rolling_count = []
    for idx in range(1, by_zip_subset.shape[0] + 1):
        by_zip_median_subset = by_zip_subset.iloc[:idx, :].copy()
        amounts_for_median = \
        by_zip_median_subset[(by_zip_median_subset.CMTE_ID == by_zip_subset.iloc[idx - 1].CMTE_ID) & \
                             (by_zip_median_subset.ZIP_CODE == by_zip_subset.iloc[idx - 1].ZIP_CODE)]['TRANSACTION_AMT']
        rolling_median.append(np.median(amounts_for_median))
    total_num_by_zip = by_zip_subset.groupby(['CMTE_ID', 'ZIP_CODE'])['TRANSACTION_AMT'].cumcount() + 1
    rolling_sum = by_zip_subset.groupby(['CMTE_ID', 'ZIP_CODE'])['TRANSACTION_AMT'].cumsum()
    median_by_zip = pd.DataFrame(columns=['CMTE_ID'])
    median_by_zip['CMTE_ID'] = by_zip_subset['CMTE_ID']
    median_by_zip['ZIP_CODE'] = by_zip_subset['ZIP_CODE']
    median_by_zip['MEDIAN_CONTRIBUTION'] = np.round(rolling_median).astype(int)
    median_by_zip['TOTAL_NUM_OF_CONTRIBUTIONS'] = np.round(total_num_by_zip).astype(int)
    median_by_zip['TOTAL_CONTRIBUTION'] = np.round(rolling_sum).astype(int)
    return median_by_zip


def create_by_date_df(contrib_df):
    '''
    This function creates a dataframe containing the median contributions,
    number of contributors, and total contribution amount grouped by date.
    :param contrib_df: Dataframe containing the relevant donor data
    :return: median_by_date: Dataframe sorted and containing the totals by date
    '''
    by_date_subset = contrib_df[contrib_df['TRANSACTION_DT'].str.len() == 8].copy()
    by_date_subset.sort_values(by=['CMTE_ID', 'TRANSACTION_DATETIME'], inplace=True)
    total_num_by_date = by_date_subset.groupby(['CMTE_ID', 'TRANSACTION_DT'])['TRANSACTION_AMT'].count()
    total_contributions_by_date = by_date_subset.groupby(['CMTE_ID', 'TRANSACTION_DT'])['TRANSACTION_AMT'].sum()
    median_vals_date = by_date_subset.groupby(['CMTE_ID', 'TRANSACTION_DT'])['TRANSACTION_AMT'].median()
    median_by_date = pd.DataFrame(columns=['MEDIAN_CONTRIBUTION'])
    median_by_date['MEDIAN_CONTRIBUTION'] = np.round(median_vals_date).astype(int)
    median_by_date['TOTAL_NUM_OF_CONTRIBUTIONS'] = np.round(total_num_by_date).astype(int)
    median_by_date['TOTAL_CONTRIBUTION'] = np.round(total_contributions_by_date).astype(int)
    median_by_date = median_by_date.reset_index().copy()
    return median_by_date


def dataframe_to_piped_outputfile(df, filename):
    '''
    This outputs a pipe-delimited text file with the given filename/path
    based on an input dataframe.
    :param df: Input dataframe
    :param filename: The name of the output file
    '''
    f = open(filename, 'w')
    df_values = df.values
    for ix in range(df_values.shape[0]):
        out_line = '|'.join(df_values[ix, :].astype(str)) + '\n'
        f.write(out_line)
    f.close()

def output_donor_files(itcont_filepath,filename1, filename2):
    '''
    This function takes a filepath as an input and outputs (1) a file containing
    running medians and totals organized by zip code, and (2) a file containing medians
    and totals organized by date.
    :param itcont_filepath: Filepath/name of the input text file containing election donor data
    outputs: 'medianvals_by_zip.txt': a file containing running medians and totals organized by zip code
             'medianvals_by_date.txt': a file containing medians and totals organized by date
    '''
    contrib_df = create_contrib_df(itcont_filepath)
    median_by_zip2 = create_by_zip_df(contrib_df)
    median_by_date2 = create_by_date_df(contrib_df)

    dataframe_to_piped_outputfile(median_by_zip2, filename1)
    dataframe_to_piped_outputfile(median_by_date2, filename2)

if __name__ == "__main__":
    output_donor_files(sys.argv[1],sys.argv[2],sys.argv[3])