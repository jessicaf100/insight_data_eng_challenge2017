find_political_donors.py is a command-line executable that takes political contributor data, parses it, and converts it into two text files: (1) a text file showing the running medians, counts, and totals sorted by Zip code and (2) a text file showing the medians, counts, and totals by date.

Example usage: python ./src/find_political_donors.py ./input/itcont.txt ./output/medianvals_by_zip.txt ./output/medianvals_by_date.txt
